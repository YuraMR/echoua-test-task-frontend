import { until } from 'selenium-webdriver';

const timeouts = {
  delayTimeout: 3 * 1000,
  sleepTimeout: 5 * 1000,
  waitUntilTimeout: 20 * 1000,
};

const select = driver =>
  async selector => {
    try {
      await driver.wait(until.elementLocated(selector));
      const element = await driver.findElement(selector);
      await driver.wait(until.elementIsVisible(element));

      return await element;
    } catch (e) {
      console.error('error', e)
    }
  };

const useElement = callback =>
  value =>
    async element => {
      try {
        await element[callback](value);
      } catch (e) {
        console.error('error', e)
      }
    };

// const fillInput = useElement('sendKeys');
const clickButton = useElement('click')(null);

const test = driver =>
  async (selector, callback) => {
    try {
      const selectElement = await select(driver);
      const element = await selectElement(selector, callback);
      await callback(element);
      await driver.sleep(timeouts.delayTimeout);
    } catch (e) {
      console.error('error', e)
    }
  };

export {
  timeouts, select, useElement, clickButton, test
}
