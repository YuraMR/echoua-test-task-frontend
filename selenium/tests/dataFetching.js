import { By, until } from 'selenium-webdriver';

export default async ({ driver, testButton }) => {
  try {
    await console.info("Start data fetching test");

    const testFetchButton = async id => {
      await console.info(`Start fetched data by id=${id}`);
      await testButton(By.id(id));
      await driver.wait(until.elementLocated(By.css('table')));
      await console.info(`Data fetched by id=${id} successfully`);
    };

    await testFetchButton('10000');
    await testFetchButton('1000');
    await testFetchButton('100');
  } catch (e) {
    console.error('error', e);
  }
  finally {
    console.info("Data fetching test finished")
  }
};
