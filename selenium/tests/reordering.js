import { By } from 'selenium-webdriver';
import co from 'co';

const headerSelectors = [
  'own_casualties',
  'enemy_casualties',
  'date',
  'location',
  'armor_costs',
  'battle_won',
].reduce((result, item) => (
  result[item] = By.id(item), result
), {});

export default async ({ testButton }) => {
  try {
    await console.info("Start reordering test");

    const reorderTable = function* () {
      for (const selector in headerSelectors)
        if (headerSelectors.hasOwnProperty(selector))
          yield testButton(headerSelectors[selector])
            .then(() => console.info(`Table reordered by ${selector} field`))
    };

    await co(reorderTable);
  } catch (e) {
    console.error('error', e);
  }
  finally {
    console.info("Reordering test finished")
  }
};
