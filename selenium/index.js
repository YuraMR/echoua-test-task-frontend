import { Builder } from 'selenium-webdriver';
import co from 'co';

import { timeouts, clickButton, test } from './utils';
import reordering from './tests/reordering';
import dataFetching from './tests/dataFetching';

const { sleepTimeout } = timeouts;

const SELENIUM_HOST = 'http://localhost:3000/';

const features = {
  reordering,
  data_fetch: dataFetching
};

const args = process.argv.slice(2);

const chromeDriver = new Builder().forBrowser('chrome').build();

const runSeleniumTest = async driver => {
  await driver.get(SELENIUM_HOST);

  const testWebElement = await test(driver);

  const testButton = async selector =>
    await testWebElement(selector, clickButton);

  const testFeature = async feature => {
    await feature({ driver, testButton });
    await driver.sleep(sleepTimeout);
  };

  const testFeatures = function* (args) {
    if (!!args.length) {
      for (const key in args) {
        if (args.hasOwnProperty(key) && features.hasOwnProperty(args[key]))
          yield testFeature(features[args[key]]);
      }
    } else {
      for (const key in features) {
        if (features.hasOwnProperty(key))
          yield testFeature(features[key])
      }
    }
  };

  try {
    await co(testFeatures(args));
  } catch (e) {
    console.error('error', e);
  }
  finally {
    await driver.sleep(sleepTimeout);
    await driver.quit();
  }
};

runSeleniumTest(chromeDriver)
  .then(() =>
    console.info("All tests finished")
  );

export default runSeleniumTest;
