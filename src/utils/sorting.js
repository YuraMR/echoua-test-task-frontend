const desc = (a, b, orderBy) => {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

const getArrayEntries = array => Object.entries(array);

const stableSort = (array, getSorting) => {
  const arrayEntries = getArrayEntries(array);

  arrayEntries.sort((a, b) => {
    const order = getSorting(a[1], b[1]);

    return order !== 0 ?
      order : a[0] - b[0]
  });

  return arrayEntries.map(item => item[1]);
};

const getSorting = (order, orderBy) =>
  order === 'desc' ?
    (a, b) => desc(a, b, orderBy)
    : (a, b) => - desc(a, b, orderBy);

export { getSorting, stableSort }
