const compose = (...functions) => value => functions.reduceRight(
  (result, item) => item(result), value
);

// const pipe = (...functions) => value => functions.reduce(
//   (result, item) => item(result), value
// );

const countKeyOccurrences = (array = []) =>
  array.reduce((result, item) => (
    Object.keys(item).forEach(key => {
      result[key] = (result[key] || 0) + 1;
    }), result
  ), {});

const calcFrequency = (frequency = 1) => frequency > 0.2;

const getOccurrenceFrequencies = (array = []) => {
  const { length } = array;
  const occurrences = countKeyOccurrences(array);

  return Object.keys(occurrences).reduce((result, item) => (
    result[item] = occurrences[item] / length, result
  ), {});
};

const getKeys = bool => (array = []) => {
  const occurrenceFrequencies = getOccurrenceFrequencies(array);

  const calcOccurrenceFrequencies = key =>
    calcFrequency(occurrenceFrequencies[key]);

  const getCondition = key =>
    bool ? calcOccurrenceFrequencies(key) : !calcOccurrenceFrequencies(key);

  return Object.keys(occurrenceFrequencies).reduce((result, item) =>
      getCondition(item) ?
        [...result, item] : result
    , []);
};

const getAcceptableKeys = getKeys(true);

// const getUnAcceptableKeys = getKeys(false);

// const removeRareKeys = array =>
//   array.map(item => (
//     getUnAcceptableKeys(array).forEach(key => {
//       delete item[key];
//     }), item)
//   );

export { compose, getAcceptableKeys }
