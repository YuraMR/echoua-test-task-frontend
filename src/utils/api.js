import axios from "axios";

const callApi = (method = 'get') => slug =>
  axios[method](`http://localhost:8080/api/${slug}`)
    .then(({ data }) => data);

const getData = callApi();

const getReports = id =>
  getData(`report/${id}`);

export { getReports }
