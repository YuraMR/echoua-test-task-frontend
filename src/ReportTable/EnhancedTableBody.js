import React from "react";
import { TableBody, TableCell, TableRow } from "@material-ui/core/umd/material-ui.production.min";

import { convertBoolean } from "../utils/stringTransformations";

const EnhancedTableBody = ({ sortedReports, reportKeys }) => (
  <TableBody>
    {sortedReports.map((report, index) => (
      <TableRow key={index}>
        <TableCell key={0}>{index + 1}</TableCell>
        {reportKeys.map((key, index) => (
          <TableCell key={index + 1}>
            {typeof report[key] === 'boolean' ?
              convertBoolean(report[key]) : report[key]
            }
          </TableCell>
        ))}
      </TableRow>
    ))}
  </TableBody>
);

export default EnhancedTableBody
