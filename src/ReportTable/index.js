import React from "react";
import { Table } from "@material-ui/core/umd/material-ui.production.min";
import EnhancedTableHead from "./EnhancedTableHead";
import EnhancedTableBody from "./EnhancedTableBody";
import { makeReadable} from "../utils/stringTransformations";
import { getSorting, stableSort } from "../utils/sorting";

class ReportTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'number',
  };

  handleRequestSort = newOrderBy => () => {
    const { order, orderBy } = this.state;

    const condition = orderBy === newOrderBy && order === 'desc';

    const newOrder = condition ? 'asc' : 'desc';

    this.setState({ order: newOrder, orderBy: newOrderBy });
  };

  render () {
    const { reports, reportKeys } = this.props;
    const { order, orderBy } = this.state;

    const headerCells = ["number", ...reportKeys].map((key, index) => ({
      key: key,
      id: index,
      label: makeReadable(key)
    }));

    const sortedReports = stableSort(reports, getSorting(order, orderBy));

    return (
      <Table>
        <EnhancedTableHead
          headerCells={headerCells}
          order={order}
          orderBy={orderBy}
          onRequestSort={this.handleRequestSort}
        />
        <EnhancedTableBody
          sortedReports={sortedReports}
          reportKeys={reportKeys}
        />
      </Table>
    )
  }
}

export default ReportTable;
