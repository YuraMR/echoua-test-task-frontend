import React from "react";
import {
  TableCell, TableHead, TableRow, TableSortLabel,
  Tooltip
} from "@material-ui/core/umd/material-ui.production.min";

const EnhancedTableHead = ({ onRequestSort, order, orderBy, headerCells }) => (
  <TableHead>
    <TableRow>
      {headerCells.map((cell, index) => (
        <TableCell
          key={cell.key}
          sortDirection={orderBy === index ? order : false}
        >
          <Tooltip
            title="Sort"
            enterDelay={100}
          >
            <TableSortLabel
              id={cell.key}
              active={orderBy === cell.key}
              direction={order}
              onClick={onRequestSort(cell.key)}
            >
              {cell.label}
            </TableSortLabel>
          </Tooltip>
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
);

export default EnhancedTableHead;
