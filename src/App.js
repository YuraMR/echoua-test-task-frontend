import React, { Component } from 'react';

import './App.css';

import { getReports } from './utils/api';
import { getAcceptableKeys } from './utils/common';
import ReportTable from './ReportTable';

class App extends Component {
  state = {
    loading: false,
  };

  componentDidMount() {
    getReports(100).then(reports =>
      this.setState({ reports })
    );
  }

  handleClick = reportId => () => {
    this.setState(prevState => ({...prevState, loading: true }));

    getReports(reportId).then(reports =>
      this.setState({ reports, loading: false })
    );
  };

  render() {
    const { reports, loading } = this.state;

    const reportKeys = getAcceptableKeys(reports);

    return (
      <div className="App">
        {[100, 1000, 10000].map((item, id) => (
          <button key={id} onClick={this.handleClick(item)} id={item}>{item}</button>
        ))}
        {reports && !loading ? <ReportTable reports={reports} reportKeys={reportKeys}/>
          : <div>Wait until all reports download</div>}
      </div>
    );
  }
}

export default App;
