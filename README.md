# echoua-test-task-frontend

## Prerequisites
- [node.js](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)
- installed and runned [backend](https://bitbucket.org/YuraMR/echoua-test-task-backend)
- [ChromeDriver](http://chromedriver.chromium.org/) (Install by one of ways described [here](https://www.kenst.com/2015/03/installing-chromedriver-on-mac-osx/) (for macOS users)
                                                     or [here](https://www.kenst.com/2019/02/installing-chromedriver-on-windows/) (for Windows users))

## Get Started

1. Install dependencies:
    ```bash
    yarn install
    yarn postinstall
    ```

2. Run in development mode:
    ```bash
    yarn start
    ```

3. Production build process:
    ```bash
    yarn build
    ```
## Run e2e test
### Run all tests
1. Run following command on terminal:
      ```bash
      yarn start:e2e
      ```
### Run specific tests
1. Choose features that you want to test:
     ```ecmascript 6
    const features = {
      reordering,
      data_fetch: dataFetching
    };
    ```
2. Use keys of chosen features as node command line arguments. Example: 
      ```bash
      yarn start:e2e data_fetch
      ```

